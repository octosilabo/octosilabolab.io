---
layout: page
title: "PROXIMAMENTE"
permalink: /next/
image: images/ PROXIMAMENTE.jpg
comments: true 
---

![#piloto]({{ site.baseurl }}/images/PROXIMAMENTE.jpg)


2° TEMPORADA 2020

Tendremos entrevistas a cultores de Cuba, Colombia, Ecuador, Chile y Argentina.

También nos acompañarán músicos de Puerto Rico y Cuba.
